<?php
namespace SDI;

class Permissions
{
  public static function verify($flag, $user_id=FALSE){
    $config = \Rhonda\Config:: get('system');
    
    if(!property_exists($config, "support_services") || empty($config->support_services->elguapo)){
      throw new \Exception("Invalid configuration value. Looking for property system->support_services->elguapo for permissions verification route.");
    }

    $headers = \Rhonda\Headers:: getallheaders();
    $user_id = (!$user_id)? \SDI\Token:: unpack($headers['Authorization'])['user_id'] : $user_id;

    $api = new \Rhonda\APIGateway('GET',$config->support_services->elguapo.'/authenticate/verify/user/'.$user_id.'/flag/'.$flag);

    return $api->run();
  }

}