<?php
namespace SDI;

class Ownership
{
  /**
  * Verify if the user has access to this group
  *
  * @param String - Group ID
  * @param String - User ID
  * @param Bool - True / False
  *
  * @example
  * <code>
  * \SDI\Ownership:: verify_group($group_id, $user_id, $throw_exception)
  * </code>
  *
  * @return Return **Object** With result if true
  *
  * @since   2016-07-25
  * @author  Wesley Dekkers <wesley@sdicg.com> 
  **/
  public static function verify_group($group_id, $user_id=FALSE, $throw_exception=TRUE){
    $config = \Rhonda\Config:: get('system');
    
    if(!property_exists($config, "support_services") || empty($config->support_services->organizations)){
      throw new \Exception("Invalid configuration value. Looking for property system->support_services->organizations for ownership verification route.");
    }

    $headers = \Rhonda\Headers:: getallheaders();
    $user_id = (!$user_id)? \SDI\Token:: unpack($headers['Authorization'])['user_id'] : $user_id;

    
    $api = new \Rhonda\APIGateway('GET',$config->support_services->organizations.'/organization/group/'.$group_id.'/user/'.$user_id.'/ownership/');

    return $api->run($throw_exception);
  }

  /**
  * Verify if the user has access to this organization
  *
  * @param String - Organization ID
  * @param String - User ID
  * @param Bool - True / False
  *
  * @example
  * <code>
  * \SDI\Ownership:: verify_organization($organization_id, $user_id, $throw_exception)
  * </code>
  *
  * @return Return **Object** With result if true
  *
  * @since   2016-07-25
  * @author  Wesley Dekkers <wesley@sdicg.com> 
  **/
  public static function verify_organization($organization_id, $user_id=FALSE, $throw_exception=TRUE){
    $config = \Rhonda\Config:: get('system');
    
    if(!property_exists($config, "support_services") || empty($config->support_services->organizations)){
      throw new \Exception("Invalid configuration value. Looking for property system->support_services->organizations for ownership verification route.");
    }

    $headers = \Rhonda\Headers:: getallheaders();
    $user_id = (!$user_id)? \SDI\Token:: unpack($headers['Authorization'])['user_id'] : $user_id;

    $api = new \Rhonda\APIGateway('GET',$config->support_services->organizations.'/organization/'.$organization_id.'/user/'.$user_id.'/ownership/');

    return $api->run($throw_exception);
  }


  /**
  * Verify if user has ownership over all groups
  *
  * @param Array - groups
  * @param String - User ID
  * @param Bool - True / False
  *
  * @example
  * <code>
  * \SDI\Ownership:: search_group_ownership($groups, $user_id, $throw_exception)
  * </code>
  *
  * @return Return **Object** With result if true
  *
  * @since   2016-08-31
  * @author  Wesley Dekkers <wesley@sdicg.com> 
  **/
  public static function search_group_ownership($groups = FALSE, $user_id=FALSE, $throw_exception=TRUE){
    $config = \Rhonda\Config:: get('system');
    
    if(!property_exists($config, "support_services") || empty($config->support_services->organizations)){
      throw new \Exception("Invalid configuration value. Looking for property system->support_services->organizations for ownership verification route.");
    }

    if(!is_array($groups)){
      throw new \Exception("No valid group(s) given");
    }

    $headers = \Rhonda\Headers:: getallheaders();
    $user_id = (!$user_id)? \SDI\Token:: unpack($headers['Authorization'])['user_id'] : $user_id;

    // Create an object to send in
    $group = new \stdClass();
    $group->group_id = $groups;

    $api = new \Rhonda\APIGateway('GET',$config->support_services->organizations.'/organization/user/'.$user_id.'/ownership/?q='.json_encode($group));

    return $api->run($throw_exception);
  }

  /**
  * List all the groups and / or organizations the user is part of and has permission too
  *
  * @param String - Type what do i want to see all the group ids, organization ids or the complete call
  * @param String - User ID
  * @param Bool - True / False
  *
  * @example
  * <code>
  * \SDI\Ownership:: list_ownership($type, $user_id, $throw_exception)
  * </code>
  *
  * @return Return **Array** of objects
  *
  * @since   2016-09-13
  * @author  Wesley Dekkers <wesley@sdicg.com> 
  **/
  public static function list_ownership($type = FALSE, $user_id=FALSE, $throw_exception=TRUE){
    $config = \Rhonda\Config:: get('system');
    
    if(!property_exists($config, "support_services") || empty($config->support_services->organizations)){
      throw new \Exception("Invalid configuration value. Looking for property system->support_services->organizations for ownership verification route.");
    }

    $headers = \Rhonda\Headers:: getallheaders();
    $user_id = (!$user_id)? \SDI\Token:: unpack($headers['Authorization'])['user_id'] : $user_id;

    // Api Call
    $api = new \Rhonda\APIGateway('GET',$config->support_services->organizations.'/organization/user/'.$user_id.'/ownership/');
    $data = $api->run($throw_exception);

    // Do we modify returning data?
    switch ($type) {

      // List all the groups where the user has access too
      case "group":
        $groups = array();
        foreach ($data->data as $item) {
          if(!empty($item->group_id)){
            $groups[] = $item->group_id;
          }
        }
        $result = $groups;
        break;

      // List all the organizations the user has access too
      case "organization":
        $organizations = array();
        foreach ($data->data as $item) {
          if(!empty($item->organization_id)){
            $organizations[] = $item->organization_id;
          }
        }

        // Only return unique organizations
        $result = array_values(array_unique($organizations));
        break;

      // Return complete API call
      default:
        $result = $data;
        break;
    }

    return $result;
  }

  /**
  * Return an array to which ids the user has ownership over (filtered potentially by a search query)
  *
  * @param String/Array - group ID / IDS
  * @param Array - Groups where you have ownership over  
  * @param String - group/organizations
  * @param Bool - True / False
  * @param Bool - True / False
  *
  * @example
  * <code>
  * \SDI\Ownership:: filter_by_ownership($group, $group_2, $exception)
  * </code>
  *
  * @return Return **Object** returns an array of IDS
  *
  * @since   2016-09-18
  * @author  Wesley Dekkers <wesley@sdicg.com> 
  **/
  public function filter_by_ownership($query_property, $ownership_items = FALSE, $ownership_type = FALSE, $exception = TRUE, $summary = TRUE){
    if(!$ownership_type){
      throw new \Exception("No ownership type selected", 400);
    }

    // if value is a string make it an array
    if(!is_array($query_property)){
      $query_property = array($query_property);
    }

    if(!$ownership_items){
      $ownership_items = Self:: list_ownership($ownership_type); 
    }

    // Compare and if you do not have access to a single one of them throw an error (soft)
    $groups = array();
    foreach($query_property as $item){
      if(in_array($item, $ownership_items)){
        $groups[] = $item;
      }else{
        // When not in array
        if ($exception) {
          // When exception is true throw hard error
          throw new \Exception("You do not have sufficient ownership to access $ownership_type with ID {$item}", 400);
        }
        else{
          if($summary){
            // else throw soft error
            Self::add_error_summary_item($ownership_type, $item);
          }
        }
      }

    }
    return $groups;
  }

  /**
  * A quick way to add a standard ownership error message to the error summary
  *
  * @param type - organization/group/etc
  * @param id - id of the resource attempted to be accessed
  *
  * @example
  * <code>
  * \SDI\Ownership:: add_error_summary_item("group","UUID");
  * </code>
  *
  * @return void
  *
  * @since   2016-09-15
  * @author  Deac Karns <deac@sdicg.com> 
  **/
  public static function add_error_summary_item($type='undefined', $id='undefined') {
    \Rhonda\Error:: add_summary_item(
      array(
          "code"=>400
        , "message"=>"You do not have sufficient ownership to access $type with ID $id"
      )
    );
  }

}
