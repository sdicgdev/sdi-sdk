<?php
namespace SDI;

class Token
{
  public static function unpack($token) {
    $token = base64_decode($token);
    $components = explode(':', $token);
    if (count($components) == 3) {
      $unpacked['user_id'] = $components[0];
      $unpacked['timestamp'] = intval($components[1]);
      $unpacked['signature'] = $components[2];
    }else{
      throw new \Exception("Authentication Error: Malformed authentication token");
    }
    return $unpacked;
  }
}