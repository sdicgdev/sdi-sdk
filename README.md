#sdi-sdk
sdi-sdk is a composer installable package that provides solutions to common SDI Core services PHP tasks.

##Install
```shell
  composer require sdicgdev/sdi-sdk:~1
  composer install
```

##Require in your project
Add the folowing to your composer.json file
```php
  "require": {
    "sdicgdev/sdi-sdk": "~1"
  }
```

##Dependencies
sdi-sdk requires some support functions in the composer package [`peledies/rhonda`](https://github.com/peledies/rhonda) which is resolved as a dependency of this sdk

##Configuration Files
The methods outlined below may require a configuration JSON file to be loaded in order to use the SDK. This allows you to have a more dynamic development environment with values that are different than your production environment. The configuration file will typically live in the `etc` directory in your project root.

The configuration file should have a similar structure:
```
{
  "scheme": "http://",
  "host": "events.eventlink.local",
  "port": 80,
  "path": "/",
  "ssl":false,
  "headers":{
    "Content-Type": "application/json"
  },
  "domain_id":"domain_2",
  "support_services":{
    "elguapo": "http://elguapo.eventlink.local"  // REQUIRED FOR PERMISSIONS VERIFICATION
  }
}
```

###Loading the configuration file
```
\Rhonda\Config:: load_file("system","path/to/file.json");
```

#Classes and Methods


Method  | Description
------------- | -------------
`verify()`  | Verify a particular permissions flag



```php
try{
  \SDI\Permissions::verify('calendar_list');
}catch(\Exception $e){
  echo \Rhonda\Error::handle($e);
}
```